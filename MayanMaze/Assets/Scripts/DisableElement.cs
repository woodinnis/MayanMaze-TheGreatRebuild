﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DisableElement : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        if (!FindObjectOfType<Tutorial>())
        {
            gameObject.SetActive(false);
        }
    }
}
