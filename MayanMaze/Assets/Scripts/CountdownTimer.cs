﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System.Collections;

public class CountdownTimer : MonoBehaviour {

    [SerializeField] float countdownTimeInSeconds;
    [SerializeField] float countdownTargetInSeconds;
    [SerializeField] int startAtSceneIndex;

    bool isPaused = false;

        // Update is called once per frame
    void Update()
    {

        //  Verify that the current scene is at or beyond the first scene requiring a timer
        if (SceneManager.GetActiveScene().buildIndex >= startAtSceneIndex)
        {

            if ((countdownTimeInSeconds > countdownTargetInSeconds) && !isPaused)
            {
                countdownTimeInSeconds-=Time.deltaTime;
            }

        }
    }
    public float GetCurrentCountdownTime()
    {
        return countdownTimeInSeconds;
    }

    public float GetTargetCountdownTime()
    {
        return countdownTargetInSeconds;
    }

    public void PauseCountdownTimer()
    {
        isPaused = true;
    }

    public void UnpauseCountdownTimer()
    {
        isPaused = false;
    }
}
