﻿using UnityEngine;
using UnityEngine.UI;
using UnityEditor;
using System.Collections;
using UnityEngine.SceneManagement;

public class Tutorial : MonoBehaviour {

    [SerializeField] GameObject textObject;
    [SerializeField] Text tutorialText;
    [SerializeField] SceneAsset[] tutorialScene;
    [SerializeField] Text[] tutorialMessages;
    [SerializeField] GameObject continueButton;

    CountdownTimer countdownTimer;
    LevelManager levelManager;
    Button button;

    // Use this for initialization
    void Start()
    {

        tutorialText = textObject.GetComponent<Text>();
        countdownTimer = FindObjectOfType<CountdownTimer>();
        levelManager = FindObjectOfType<LevelManager>();
        button = continueButton.GetComponent<Button>();

        countdownTimer.PauseCountdownTimer();

        DisplayCurrentTutorial();
    }

    // Show the correct tutorial message for the current level
    private void DisplayCurrentTutorial()
    {
        string currentLevel = levelManager.GetCurrentLevelName();
        int size = tutorialScene.Length;
        for (int i = 0; i < size; i++)
        {
            if(currentLevel == tutorialScene[i].name)
            {
                tutorialText.text = tutorialMessages[i].text;
            }
        }
    }

    // Clear the tutorial message, and unpause the game
    public void TutorialContinueButtonClick()
    {
        tutorialText.text = "";
        countdownTimer.UnpauseCountdownTimer();
        DisableTutorial();
    }

    //  Disable the tutorial message
    private void DisableTutorial()
    {
        continueButton.SetActive(false);
    }
}
