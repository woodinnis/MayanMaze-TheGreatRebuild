﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class UI_Timer : MonoBehaviour
{
    [SerializeField] string countdownMessage = "Start In: ";

    CountdownTimer countdownTimer;
    float currentCountdownTime = 0f;
    //Text text;
    TextMeshProUGUI text;

    // Start is called before the first frame update
    void Start()
    {
        countdownTimer = FindObjectOfType<CountdownTimer>();
        text = GetComponent<TextMeshProUGUI>();
    }

    // Update is called once per frame
    void Update()
    {
        currentCountdownTime = Mathf.Round(countdownTimer.GetCurrentCountdownTime());

        if (currentCountdownTime <= 0)
        {
            text.enabled = false;
        }
        else
        {
            text.text = countdownMessage + currentCountdownTime.ToString();
        }
    }
}
