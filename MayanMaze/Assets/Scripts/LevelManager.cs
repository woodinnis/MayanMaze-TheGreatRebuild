﻿using UnityEngine;
using UnityEditor;
using UnityEngine.SceneManagement;
using System.Collections;
using System.Collections.Generic;

public class LevelManager : MonoBehaviour
{

    [SerializeField] float autoLoadNextLevelAfter;
    [SerializeField] SceneAsset startScene;
    [SerializeField] SceneAsset finalScene;
    [SerializeField] List<string> Levels = new List<string>();

    [SerializeField]
    int nextSceneBuildIndex;
    [SerializeField]
    int finalSceneBuildIndex;

    int levelListSize = 0;

    void Awake()
    {
        if (GameObject.FindObjectsOfType<LevelManager>().Length > 1)
        {
            Destroy(gameObject);
        }
        DontDestroyOnLoad(gameObject);
    }



    void Start() {
        // Wait for the Splash Screen to time out before launching the Start Menu
        StartCoroutine(LoadNextScene(autoLoadNextLevelAfter));
    }

    public IEnumerator LoadNextScene(float loadSceneInSeconds)
    {
        yield return new WaitForSeconds(loadSceneInSeconds);
        if(SceneManager.GetActiveScene().buildIndex == 0)
        {
            LoadStartMenu();
        }
        else
        {
            LoadNextLevel();
        }
    }

    public void LoadStartMenu()
    {
        SceneManager.LoadScene(startScene.name);
    }

    public void LoadNextLevel()
    {
        int buildIndex = SceneManager.GetActiveScene().buildIndex;
        SceneManager.LoadScene(buildIndex + 1);
    }

    //    /// <summary>
    //    /// All functions in the GetCurrentFunctions region are used to obtain and return current level information
    //    /// </summary>
    //    #region GetCurrentFunctions

    public int GetCurrentLevelIndex()
    {
        return SceneManager.GetActiveScene().buildIndex;
    }

    public string GetCurrentLevelName()
    {
        string currentLevelName = SceneManager.GetActiveScene().name;

        return currentLevelName;
    }

//    public int GetCurrentLevelNumber()
//    {
//        string currentLevelName = GetCurrentLevelName();
//        int currentLevelIndex = 0;

//        // Verify index of current level in the Levels list
//        foreach (string level in Levels)
//        {
//            if (level.Contains(currentLevelName))
//            {
//                currentLevelIndex = Levels.IndexOf(level);
//            }
//        }
//        return currentLevelIndex;
//    }

//    #endregion

//    #region NextLevelFunctions

//    /// <summary>
//    /// If incrementing the currentLevelIndex by 1 does not go beyond the Levels list capacity 
//    /// assign nextLevelIndex the value of currentLevelIndex + 1
//    /// </summary>
//    /// <returns>nextLevelIndex</returns>
//    int GetNextLevelIndex()
//    {
//        int currentLevelIndex = GetCurrentLevelNumber();
//        int nextLevelIndex = 0;

//        if (currentLevelIndex + 1 < Levels.Capacity)
//        {
//            nextLevelIndex = currentLevelIndex + 1;
//        }

//        return nextLevelIndex;
//    }

//    /// <summary>
//    /// If the value returned by GetCurrentLevelName() does not match finalScene, procede to the next level, 
//    /// otherwise return to the main menu
//    /// </summary>
//    public void LoadNextLevel(){

//        Debug.Log("Levels List Size: " + levelListSize);

//        if (GetCurrentLevelName() != finalScene)
//        {
//            int nLevel = GetNextLevelIndex();
//            Debug.Log("GetNextLevelIndex Returned " + nLevel);

//            string nextScene = Levels[nLevel];
//            SceneManager.LoadScene(nextScene);
//        }
//        else
//            LoadStartMenu();
//    }

//    #endregion

//    public void RestartLevel()
//    {
//        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
//    }
 
}
