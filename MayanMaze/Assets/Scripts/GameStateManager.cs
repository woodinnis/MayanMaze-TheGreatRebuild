﻿using UnityEngine;
using System.Collections;

public class GameStateManager : MonoBehaviour {

    public enum GameState { MENU, PLAY, PAUSE, COMPLETE, GAMEOVER};

    GameState gameState;

    LevelManager levelManager;

    // Keep the GameStateManager in all subsequent scenes
    private void Awake()
    {
        if(GameObject.FindObjectsOfType<GameStateManager>().Length > 1)
        {
            Destroy(gameObject);
        }

        DontDestroyOnLoad(gameObject);
    }

    private void Start()
    {
        levelManager = FindObjectOfType<LevelManager>();
    }

    public GameState GetGameState()
    {
        return gameState;
    }

    public void SetGameState(GameState newGameState)
    {
        // nothing to see here yet
    }

}
