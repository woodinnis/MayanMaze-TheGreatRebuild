﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;

public class GameUI : MonoBehaviour {

    public RectTransform GameUI_RectTransform;
    public LevelManager levelManager;

	// Use this for initialization
	public void Awake () {

        levelManager = FindObjectOfType<LevelManager>();
        GameUI_RectTransform = GetComponent<RectTransform>();

	}
}
