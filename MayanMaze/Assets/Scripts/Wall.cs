﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Wall : MonoBehaviour
{
    [SerializeField] BoxCollider2D wallCollider;

    private void Start()
    {
        //wallCollider = GetComponent<BoxCollider2D>();
    }

    // Disable collision, preventing the player from being pushed away from the tile
    private void OnCollisionEnter2D(Collision2D collision)
    {
        if(collision.gameObject.tag == "Player")
        {
            Debug.Log("Sorry I can't move for you");
        }
        else if(collision.gameObject.tag == "Door")
        {
            Debug.Log("You're a door, I'll get out of the way");
            wallCollider.enabled = false;
        }
    }
}
